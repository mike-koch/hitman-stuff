﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileStitcher
{
    class Program
    {
        private const int TileHeight = 256;
        private const int TileWidth = 256;

        static void Main(string[] args)
        {
            var hitmapsRepoPath = @"E:\Sandbox\PHP\hitman-maps";

            var directories = new List<string>
            {
                @"api\maps\bangkok\tiles\0\5",
                @"api\maps\bangkok\tiles\1\5",
                @"api\maps\bangkok\tiles\2\5",
                @"api\maps\bangkok\tiles\3\5",
                @"api\maps\bangkok\tiles\4\5",
                @"api\maps\bangkok\tiles\5\5",
                @"api\maps\colorado\tiles\0\5",
                @"api\maps\colorado\tiles\1\5",
                @"api\maps\colorado\tiles\2\5",
                @"api\maps\colorado\tiles\3\5",
                @"api\maps\freeform-training\tiles\0\5",
                @"api\maps\freeform-training\tiles\1\5",
                @"api\maps\freeform-training\tiles\2\5",
                @"api\maps\freeform-training\tiles\3\5",
                @"api\maps\hokkaido\tiles\0\5",
                @"api\maps\hokkaido\tiles\1\5",
                @"api\maps\hokkaido\tiles\2\5",
                @"api\maps\hokkaido\tiles\3\5",
                @"api\maps\marrakesh\tiles\0\5",
                @"api\maps\marrakesh\tiles\1\5",
                @"api\maps\marrakesh\tiles\2\5",
                @"api\maps\marrakesh\tiles\3\5",
                @"api\maps\paris\tiles\0\5",
                @"api\maps\paris\tiles\1\5",
                @"api\maps\paris\tiles\2\5",
                @"api\maps\paris\tiles\3\5",
                @"api\maps\sapienza\tiles\0\5",
                @"api\maps\sapienza\tiles\1\5",
                @"api\maps\sapienza\tiles\2\5",
                @"api\maps\sapienza\tiles\3\5",
                @"api\maps\sapienza\tiles\4\5",
                @"api\maps\sapienza\tiles\5\5",
                @"api\maps\sapienza\tiles\6\5",
                @"api\maps\sapienza\tiles\7\5",
                @"api\maps\the-final-test\tiles\0\5",
                @"api\maps\the-final-test\tiles\1\5",
                @"api\maps\the-final-test\tiles\2\5",
                @"api\maps\the-final-test\tiles\3\5",
            };

            foreach (var path in directories)
            {
                var mainDirectory = $@"{hitmapsRepoPath}\{path}";
                var firstFolder = $@"{mainDirectory}\0";

                #region Setup
                var folders = Directory.GetDirectories(mainDirectory);
                var orderedFolders = new Dictionary<int, string>();
                foreach (var folder in folders)
                {
                    orderedFolders[int.Parse(new DirectoryInfo(folder).Name)] = folder;
                }

                var fileHeight = Directory.GetFiles(firstFolder).Length * TileHeight;
                var fileWidth = folders.Length * TileWidth;
                var targetImage = new Bitmap(fileWidth, fileHeight, PixelFormat.Format32bppArgb);
                var graphics = Graphics.FromImage(targetImage);
                #endregion

                #region Processing
                for (int i = 0; i < folders.Length; i++)
                {
                    var files = Directory.GetFiles(orderedFolders[i]);
                    if (!files.Any())
                    {
                        Environment.Exit(0);
                    }

                    var orderedTiles = new Dictionary<int, string>();
                    foreach (var file in files)
                    {
                        orderedTiles[int.Parse(Path.GetFileNameWithoutExtension(file))] = file;
                    }


                    for (int j = 0; j < files.Length; j++)
                    {
                        var tile = new Bitmap(orderedTiles[j]);
                        graphics.DrawImage(tile, i * tile.Width, j * tile.Height);
                    }
                }
                #endregion

                targetImage.Save($@"{mainDirectory}\output.png", ImageFormat.Png);
            }
        }
    }
}
