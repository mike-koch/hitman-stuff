# Elusive Targets
Elusive Targets are stored under a specific code name, while the in-game name may be
different. Documented here is a running list of code names for the World of Assassination series in the game files.

| In-Game Name | Code Name | Notes |
|--------------|---------------|-------|
| The Forger | Tequila sunrize ||
| The Congressman | Mint julep | Released as part of "The Deceivers" in H2/H3 |
| The Prince | Sazerac ||
| The Gunrunner | Screwdriver ||
| The Twin | Pisco Sour ||
| The Wildcard | Quadruple Rum And Coke | Won't be released in H2 |
| The Broker | Margarita ||
| The Black Hat | Kamikaze ||
| The Fixer | Cosmopolitan ||
| The Pharmacist | Kir Royal ||
| The Identity Thief | White Russian ||
| The Ex-Dictator | Brass Monkey ||
| The Chef | Sakini ||
| The Angel of Death | Hot Toddy ||
| The Guru | Harvey Wallbanger | Released as part of "The Deceivers" in H2/H3 |
| The Food Critic | Martini | |
| The Chameleon | Bushwhacker ||
| The Blackmailer | Lumumba ||
| The Warlord | Bloody Mary ||
| The Surgeons | Moscow Mule ||
| The Bookkeeper | Flirtini ||
| The Fugitive | Dirty Octopus ||
| The Paparazzo | Mojito ||
| The Badboy | Caipirinha ||
| The Entertainer | Mai Tai ||
| The Undying | Sambuca ||
| The Undying Returns | Sambuca2 ||
| -N/A- | Ectoplasm | Someone in Mumbai - canceled ET? |
| -N/A- | Kabuki | Someone in Mumbai - canceled ET? |
| The Serial Killer | AlabamaSlammer ||
| The Politician | Adonis ||
| The Revolutionary | Highball ||
| The Deceivers | CorpseReviver ||
| The Appraiser | GoldenDoublet ||
| Jimmy Chen | SkittleBomb | Formerly known as Yin Chen |
