# Audio Files

Quick breakdown of some of the audio file formats:

`Dth_HeadLock_*_*_*`: Audio played when subduing a NPC for a prolonged period of time  
`InSt_HM2Cls_*_*_*`: Audio played when 47 is too close for a NPC's comfort

...there are hundreds more, but I'm too lazy to type them all out