# Random File Information
- File names exported by QuickBMS appear to be a partial MD5 of the original file name. Example for file "00204d1afd76ab13.REPO":

  ![](https://i.notex.app/4J8PO)