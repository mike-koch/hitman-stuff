# Missions
Missions are stored under a specific code name, while the in-game name may be
different. Documented here is a running list of code names for both HITMAN and 
HITMAN 2.

## HITMAN (2016)
| In-Game Name | Map Code Name | "Comment" Code Name | Contract Location ID |
|--------------|---------------|---------------------|----------------------|
| Arrival | Polarbear | Polarbear Arrival First Time | - |
| Guilded Training | Polarbear | Polarbear Module 002 | - |
| Freeform Training | Polarbear | Polarbear Module 002_B | 99 |
| The Final Test | Polarbear | Polarbear Graduation | 01 |
| The Showstopper | paris | The Showstopper | 02 |
| Holiday Hoarders | parisnoel | Paris Noel | 02 |
| World of Tomorrow | octopus | World of Tomorrow | 03 |
| The Author | ebola | Ebola (Sapienza) | - |
| Landslide | mamba | Landslide | 04 |
| The Icon | copperhead | Copperhead | 05 |
| A Gilded Cage | spider | Too Big For Jail | 06 |
| A House Built on Sand | python | A House Built on Sand | 07 |
| Club 27 | tiger | Tiger | 08 |
| The Source | zika | Zika (Bangkok) | 16 |
| Freedom Fighters | bull | Bull | 09 |
| The Vector | rabies | Rabies (Colorado) | - |
| Situs Inversus | snowcrane | Snow Crane | 10 | 
| Hokkaido Snow Festival | mamushi | - | - |
| Patient Zero | flu | Flu (Hokkaido) | 18 |

## HITMAN 2 (2018) 
| In-Game Name | Map Code Name | Notes | Contract Location ID |
|--------------|---------------|-------|--|
| Nightcall | sheep || 20 |
| The Finish Line | flamingo || 11 |
| The Three-Headed Serpent | hippo || 12 |
| Embrace of the Serpent | - || - |
| Chasing A Ghost | mongoose || 13 |
| Illusions of Grandeur | - || 99 |
| Another Life | skunk || 22 |
| A Bitter Pill | - || 99 |
| The Ark Society | magpie || 21 |
| Golden Handshake | raccoon | | 24 |
| The Last Resort | stingray | | 26 |

## HITMAN 3 (2021)
| In-Game Name | Map Code Name | Notes | Contract Location ID |
|--------------|---------------|-------|--|
| On Top Of The World | - || 27 |
| Death In The Family | - || 28 |
| Dartmoor Garden Festival | - || - |
| Apex Predator | - || 29 |
| End Of An Era | - || 30 |
| The Farewell | - || 31 |
| Untouchable | - || - |

## Sniper Assassin
| In-Game Name | Map Code Name | Notes |
|--------------|---------------|-------|
| The Last Yardbird | hawk | Himmelstein |
| The Pen and the Sword | seagull | Hantu Port |
| Crime and Punishment | falcon | Siberia |
